package com.finki.wpproject.wpprojectspring.persistence;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.finki.wpproject.wpprojectspring.models.entities.Rating;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository for managing ratings in an in-memory database.
 */
@Repository("ratingRepository")
@Transactional
public class InMemoryRatingRepository implements RatingRepository {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @PersistenceContext
    public EntityManager em;

    @Override
    public Optional<Rating> save(Rating rating) {
        logger.info(rating.getId() != null ? "Updating rating." : "Creating new rating.");
        return Optional.of(em.merge(rating));
    }
}
