package com.finki.wpproject.wpprojectspring.persistence;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.finki.wpproject.wpprojectspring.models.entities.Player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Repository for managing players in an in-memory database.
 */
@Repository("playerRepository")
@Transactional
public class InMemoryPlayerRepository implements PlayerRepository {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @PersistenceContext
    public EntityManager em;

    @Override
    public Optional<Player> findPlayerByUsername(String username) {
        logger.info("Trying to find player by username '{}'.", username);
        try {
            Player player = (Player) em.createQuery("select p from Player p where p.username=:username")
                    .setParameter("username", username).getSingleResult();
            logger.info("Player with username '{}' found.", username);
            return Optional.of(player);
        } catch (Exception e) {
            logger.error("Unable to find player with username '{}'.", username);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Player> save(Player player) {
        logger.info((player.getId() != null ? "Updating player with username '{}'."
                : "Saving new player with username '{}'."), player.getUsername());
        return Optional.of(em.merge(player));
    }

    @Override
    public List<Player> findPlayersByTerm(String term) {
        logger.info("Searching for players matching the term: '{}'", term);
        return em.createQuery("select p from Player p where p.username like :term or p.nickname like :term",
                Player.class).setParameter("term", "%" + term + "%").getResultList();
    }

    @Override
    public List<Player> findAll() {
        logger.info("Returning all registered players.");
        return em.createQuery("select p from Player p", Player.class).getResultList();
    }
}
