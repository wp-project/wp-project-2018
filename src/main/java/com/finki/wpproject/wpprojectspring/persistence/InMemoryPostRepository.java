package com.finki.wpproject.wpprojectspring.persistence;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.finki.wpproject.wpprojectspring.models.entities.Post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository for managing ratings in an in-memory database.
 */
@Repository("postRepository")
@Transactional
public class InMemoryPostRepository implements PostRepository {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PersistenceContext
    public EntityManager em;

    @Override
    public Optional<Post> save(Post post) {
        logger.info("Saving new post for user '{}'.", post.getOp().getUsername());
        return Optional.of(em.merge(post));
    }

}