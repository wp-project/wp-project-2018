package com.finki.wpproject.wpprojectspring.persistence;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import com.finki.wpproject.wpprojectspring.models.entities.FriendRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class InMemoryFriendRequestRepository implements FriendRequestRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private EntityManager entityManager;

    public InMemoryFriendRequestRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public FriendRequest save(FriendRequest friendRequest) {
        logger.info("Persisting friend request between users '{}' and '{}'.", friendRequest.getSentFrom().getUsername(),
                friendRequest.getSentTo().getUsername());

        return entityManager.merge(friendRequest);
    }

    @Override
    public Optional<FriendRequest> findFriendRequestById(Long id) {
        return Optional.of(entityManager.find(FriendRequest.class, id));
    }

}