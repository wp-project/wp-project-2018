package com.finki.wpproject.wpprojectspring.persistence;

import java.util.Optional;

import com.finki.wpproject.wpprojectspring.models.entities.Post;

public interface PostRepository {
    Optional<Post> save(Post post);
}