package com.finki.wpproject.wpprojectspring.persistence;

import java.util.Optional;

import com.finki.wpproject.wpprojectspring.models.entities.FriendRequest;

public interface FriendRequestRepository {
    public FriendRequest save(FriendRequest friendRequest);

    public Optional<FriendRequest> findFriendRequestById(Long id);
}