package com.finki.wpproject.wpprojectspring.persistence;

import java.util.List;
import java.util.Optional;

import com.finki.wpproject.wpprojectspring.models.entities.Player;

/**
 * An Interface defining the methods which the repository for managing players should implement.
 */
public interface PlayerRepository {
    /**
     * Tries to find a player by username and if found returns it.
     * @param username - The username with which the player is searched.
     * @return - An optional of the player object or an empty optional if no player is found.
     */
    Optional<Player> findPlayerByUsername(String username);

    /**
     * Tries to create or update an existing player.
     * @param player - The player to create or update in the database.
     * @return - The created/updated player.
     */
    Optional<Player> save(Player player);

    /**
     * Finds players matching a given term.
     * @param term - The term to be matched against the player.
     * @return - A list of players satisfying the given condition.
     */
    List<Player> findPlayersByTerm(String term);

    /**
     * Method that returns all registered players.
     * @return - List of all the registered players;
     */
    List<Player> findAll();
}
