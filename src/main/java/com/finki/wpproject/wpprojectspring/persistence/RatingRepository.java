package com.finki.wpproject.wpprojectspring.persistence;

import java.util.Optional;

import com.finki.wpproject.wpprojectspring.models.entities.Rating;

/**
 * An Interface defining the methods which the repository managing ratings should implement.
 */
public interface RatingRepository {
    /**
     * Tries to create or update a rating.
     * @param rating - The rating to create or update.
     * @return - The created/updated rating.
     */
    Optional<Rating> save(Rating rating);
}
