package com.finki.wpproject.wpprojectspring.exceptions.rating;

/**
 * Exception that indicates that the user still doesn't have a rating.
 */
public class NoRatingException extends RuntimeException {
    public NoRatingException() {
        super("User does not yet have a rating.");
    }
}