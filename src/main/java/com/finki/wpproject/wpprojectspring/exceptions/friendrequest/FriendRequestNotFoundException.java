package com.finki.wpproject.wpprojectspring.exceptions.friendrequest;

public class FriendRequestNotFoundException extends RuntimeException {
    public FriendRequestNotFoundException() {
        super("Friend request not found.");
    }

    public FriendRequestNotFoundException(Long id) {
        super(String.format("Friend request with id %d does not exist.", id));
    }
}