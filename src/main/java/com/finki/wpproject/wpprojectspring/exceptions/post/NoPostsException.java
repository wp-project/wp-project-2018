package com.finki.wpproject.wpprojectspring.exceptions.post;

/**
 * Exception indicating that the user has no posts.
 */
public class NoPostsException extends RuntimeException {
    public NoPostsException() {
        super("User has no posts.");
    }
}