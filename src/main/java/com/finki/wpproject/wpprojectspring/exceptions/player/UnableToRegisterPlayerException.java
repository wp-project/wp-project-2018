package com.finki.wpproject.wpprojectspring.exceptions.player;

/**
 * Exception indicating the there was an error saving the player in the database.
 */
public class UnableToRegisterPlayerException extends RuntimeException {
    public UnableToRegisterPlayerException() {
        super("Unable to create player.");
    }

    public UnableToRegisterPlayerException(String username) {
        super(String.format("Unable to create player with username '%s'", username));
    }
}
