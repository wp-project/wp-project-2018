package com.finki.wpproject.wpprojectspring.exceptions.rating;

/**
 * Exception indicating that a second attempt was made to submit quiz answers.
 */
public class SecondQuizAttemptException extends RuntimeException {
    public SecondQuizAttemptException() {
        super("Only one attempt at the quiz can be done by a single user.");
    }
}
