package com.finki.wpproject.wpprojectspring.exceptions.rating;

/**
 * Exception indicating that there was an error saving the rating in the database.
 */
public class UnableToSaveRatingException extends RuntimeException {
    public UnableToSaveRatingException() {
        super("Unable to save rating.");
    }
}
