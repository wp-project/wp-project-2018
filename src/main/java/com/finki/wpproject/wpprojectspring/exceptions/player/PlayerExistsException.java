package com.finki.wpproject.wpprojectspring.exceptions.player;

/**
 * Exception indicating that a player with a given username already exists in the database.
 */
public class PlayerExistsException extends RuntimeException {
    public PlayerExistsException(String username) {
        super(String.format("Player with username '%s' already exists.", username));
    }
}
