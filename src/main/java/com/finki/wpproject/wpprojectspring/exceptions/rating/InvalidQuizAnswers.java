package com.finki.wpproject.wpprojectspring.exceptions.rating;

/**
 * Exception indicating that the provided quiz answers from the frontend are invalid.
 */
public class InvalidQuizAnswers extends RuntimeException {
    public InvalidQuizAnswers() {
        super("Invalid quiz answers.");
    }
}
