package com.finki.wpproject.wpprojectspring.exceptions.common;

/**
 * General exception for an unauthorized request.
 */
public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException() {
        super("User is not authenticated.");
    }
}
