package com.finki.wpproject.wpprojectspring.exceptions.player;

/**
 * Exception indicating that there was an error updating the player in the database.
 */
public class UnableToUpdatePlayerException extends RuntimeException {
    public UnableToUpdatePlayerException() {
        super("Unable to update player.");
    }
}
