package com.finki.wpproject.wpprojectspring.exceptions.common;

public class BadRequestException extends RuntimeException {
    public BadRequestException(String s) {
        super(s);
    }

    public BadRequestException() {
        super("Bad request.");
    }
}
