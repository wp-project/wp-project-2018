package com.finki.wpproject.wpprojectspring.exceptions.friendrequest;

public class FriendRequestAlreadyExistsException extends RuntimeException {
    public FriendRequestAlreadyExistsException() {
        super("Friend request already exists.");
    }

    public FriendRequestAlreadyExistsException(String username1, String username2) {
        super(String.format("Friend request between users '%s' and '%s' already exists.", username1, username2));
    }
}