package com.finki.wpproject.wpprojectspring.exceptions.friendrequest;

public class AlreadyFriendsException extends RuntimeException {
    public AlreadyFriendsException() {
        super("Users are already friends.");
    }

    public AlreadyFriendsException(String user1, String user2) {
        super(String.format("Users '%s' and '%s' are already friends."));
    }
}