package com.finki.wpproject.wpprojectspring.models.dto;

public class MatchedPlayer {

    private String username;
    private String nickname;
    private float matching;

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return the matching
     */
    public float getMatching() {
        return matching;
    }

    /**
     * @param matching the matching to set
     */
    public void setMatching(float matching) {
        this.matching = matching;
    }
}