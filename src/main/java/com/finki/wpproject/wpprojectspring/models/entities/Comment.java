package com.finki.wpproject.wpprojectspring.models.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Comment entity.
 */
@Entity
public class Comment {

    @Id
    @GeneratedValue
    private Long id;

    @Size(max = 160)
    private String text;
    private String link;

    @NotNull
    @ManyToOne
    private Player player;

    @NotNull
    @ManyToOne
    private Post post;

    @NotNull
    private LocalDateTime commentedOn;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @param player the player to set
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * @return the post
     */
    public Post getPost() {
        return post;
    }

    /**
     * @param post the post to set
     */
    public void setPost(Post post) {
        this.post = post;
    }

    /**
     * @return the commentedOn
     */
    public LocalDateTime getCommentedOn() {
        return commentedOn;
    }

    /**
     * @param commentedOn the commentedOn to set
     */
    public void setCommentedOn(LocalDateTime commentedOn) {
        this.commentedOn = commentedOn;
    }
}
