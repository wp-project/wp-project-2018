package com.finki.wpproject.wpprojectspring.models.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Password reset entity.
 */
@Entity
public class PasswordReset {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String resetToken;
    @NotNull
    private LocalDateTime requestedOn;
    @NotNull
    private LocalDateTime usedOn;

    @NotNull
    private Boolean used;

    @ManyToOne
    private Player player;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the resetToken
     */
    public String getResetToken() {
        return resetToken;
    }

    /**
     * @param resetToken the resetToken to set
     */
    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    /**
     * @return the requestedOn
     */
    public LocalDateTime getRequestedOn() {
        return requestedOn;
    }

    /**
     * @param requestedOn the requestedOn to set
     */
    public void setRequestedOn(LocalDateTime requestedOn) {
        this.requestedOn = requestedOn;
    }

    /**
     * @return the usedOn
     */
    public LocalDateTime getUsedOn() {
        return usedOn;
    }

    /**
     * @param usedOn the usedOn to set
     */
    public void setUsedOn(LocalDateTime usedOn) {
        this.usedOn = usedOn;
    }

    /**
     * @return the used
     */
    public Boolean getUsed() {
        return used;
    }

    /**
     * @param used the used to set
     */
    public void setUsed(Boolean used) {
        this.used = used;
    }

    /**
     * @return the player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @param player the player to set
     */
    public void setPlayer(Player player) {
        this.player = player;
    }
}
