package com.finki.wpproject.wpprojectspring.models.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class PlayerQuery {

    @Min(-5)
    @Max(5)
    private float solo;

    @Min(-5)
    @Max(5)
    private float skill;
    
    @Min(-5)
    @Max(5)
    private float speech;
    
    @Min(-5)
    @Max(5)
    private float spirit;
    
    @Min(-5)
    @Max(5)
    private float sophistication;
    
    private String country;

    /**
     * @return the solo
     */
    public float getSolo() {
        return solo;
    }

    /**
     * @param solo the solo to set
     */
    public void setSolo(float solo) {
        this.solo = solo;
    }

    /**
     * @return the skill
     */
    public float getSkill() {
        return skill;
    }

    /**
     * @param skill the skill to set
     */
    public void setSkill(float skill) {
        this.skill = skill;
    }

    /**
     * @return the speech
     */
    public float getSpeech() {
        return speech;
    }

    /**
     * @param speech the speech to set
     */
    public void setSpeech(float speech) {
        this.speech = speech;
    }

    /**
     * @return the spirit
     */
    public float getSpirit() {
        return spirit;
    }

    /**
     * @param spirit the spirit to set
     */
    public void setSpirit(float spirit) {
        this.spirit = spirit;
    }

    /**
     * @return the sophistication
     */
    public float getSophistication() {
        return sophistication;
    }

    /**
     * @param sophistication the sophistication to set
     */
    public void setSophistication(float sophistication) {
        this.sophistication = sophistication;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }
}