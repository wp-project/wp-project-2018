package com.finki.wpproject.wpprojectspring.models.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Friend request entity.
 */
@Entity
public class FriendRequest {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @ManyToOne
    private Player sentFrom;
    @NotNull
    @ManyToOne
    private Player sentTo;

    @NotNull
    private Boolean pending;

    private Boolean confirmed;

    @NotNull
    private LocalDateTime sentOn;

    private LocalDateTime resolvedOn;

    public FriendRequest() {

    }

    public FriendRequest(Player loggedInPlayer, Player otherPlayer) {
        sentFrom = loggedInPlayer;
        sentTo = otherPlayer;
        pending = true;
        sentOn = LocalDateTime.now();
    }

    public void resolve(Boolean resolution) {
        pending = false;
        confirmed = resolution;
        resolvedOn = LocalDateTime.now();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the sentFrom
     */
    public Player getSentFrom() {
        return sentFrom;
    }

    /**
     * @param sentFrom the sentFrom to set
     */
    public void setSentFrom(Player sentFrom) {
        this.sentFrom = sentFrom;
    }

    /**
     * @return the sentTo
     */
    public Player getSentTo() {
        return sentTo;
    }

    /**
     * @param sentTo the sentTo to set
     */
    public void setSentTo(Player sentTo) {
        this.sentTo = sentTo;
    }

    /**
     * @return the pending
     */
    public Boolean getPending() {
        return pending;
    }

    /**
     * @param pending the pending to set
     */
    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    /**
     * @return the confirmed
     */
    public Boolean getConfirmed() {
        return confirmed;
    }

    /**
     * @param confirmed the confirmed to set
     */
    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * @return the sentOn
     */
    public LocalDateTime getSentOn() {
        return sentOn;
    }

    /**
     * @param sentOn the sentOn to set
     */
    public void setSentOn(LocalDateTime sentOn) {
        this.sentOn = sentOn;
    }

    /**
     * @return the resolvedOn
     */
    public LocalDateTime getResolvedOn() {
        return resolvedOn;
    }

    /**
     * @param resolvedOn the resolvedOn to set
     */
    public void setResolvedOn(LocalDateTime resolvedOn) {
        this.resolvedOn = resolvedOn;
    }
}
