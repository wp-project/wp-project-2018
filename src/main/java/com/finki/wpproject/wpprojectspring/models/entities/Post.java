package com.finki.wpproject.wpprojectspring.models.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Post entity.
 */
@Entity
public class Post {
    
    @Id
    @GeneratedValue
    private Long id;

    @Size(max = 160)
    private String text;
    private String link;

    @NotNull
    @ManyToOne
    private Player op;

    @OneToMany(mappedBy = "post")
    private List<Comment> comments;

    @NotNull
    private LocalDateTime postedOn;

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the op
     */
    public Player getOp() {
        return op;
    }

    /**
     * @param op the op to set
     */
    public void setOp(Player op) {
        this.op = op;
    }

    /**
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * @return the postedOn
     */
    public LocalDateTime getPostedOn() {
        return postedOn;
    }

    /**
     * @param postedOn the postedOn to set
     */
    public void setPostedOn(LocalDateTime postedOn) {
        this.postedOn = postedOn;
    }
}
