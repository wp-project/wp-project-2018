package com.finki.wpproject.wpprojectspring.models.dto;

/**
 * DTO class containing informations about a logged in user.
 */
public class LoggedInUserDTO {
    
    private String username;
    private String nickname;
    private Boolean hasCompletedQuiz;

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return the hasCompletedQuiz
     */
    public Boolean getHasCompletedQuiz() {
        return hasCompletedQuiz;
    }

    /**
     * @param hasCompletedQuiz the hasCompletedQuiz to set
     */
    public void setHasCompletedQuiz(Boolean hasCompletedQuiz) {
        this.hasCompletedQuiz = hasCompletedQuiz;
    }
}
