package com.finki.wpproject.wpprojectspring.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Player entity and UserDetails implementation.
 */
@Entity
public class Player implements UserDetails {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 2, max = 20)
    @Pattern(regexp = "^[a-zA-Z0-9]*$")
    private String username;

    @NotNull
    @Email
    private String email;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @NotNull
    @Size(min = 2, max = 20)
    @Pattern(regexp = "^[a-zA-Z0-9]*$")
    private String nickname;

    @NotNull
    private String country;
    private String filename;

    private String steamId;
    private String battleTag;
    private String originId;

    @OneToOne
    private Rating currentRating;

    @JsonIgnore
    @OneToMany(mappedBy = "madeBy")
    private List<Rating> ratingsMade;
    @JsonIgnore
    @OneToMany(mappedBy = "relatedTo")
    private List<Rating> ratingsReceived;

    @JsonIgnore
    @ManyToMany
    private List<Player> friends;

    @JsonIgnore
    @ManyToMany(mappedBy = "players")
    private List<Game> games;

    @JsonIgnore
    @OneToMany(mappedBy = "sentFrom")
    private List<FriendRequest> sentFriendRequests;
    @JsonIgnore
    @OneToMany(mappedBy = "sentTo")
    private List<FriendRequest> receivedFriendRequests;

    @JsonIgnore
    @OneToMany(mappedBy = "op")
    private List<Post> posts;

    @JsonIgnore
    @OneToMany(mappedBy = "player")
    private List<Comment> comments;

    @JsonIgnore
    @OneToMany(mappedBy = "player")
    private List<PasswordReset> passwordResets;

    @JsonIgnore
    private Boolean hasCompletedQuiz = false;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority("user");
        authorities.add(simpleGrantedAuthority);
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public String toString() {
        return String.format(
                "Username:'%s'\nPassword:'%s'\nNickname:'%s'\nSteamId:'%s'\nBattleTag:'%s'\nOriginId:'%s'\n", username,
                password, nickname, steamId, battleTag, originId);
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the currentRating
     */
    public Rating getCurrentRating() {
        return currentRating;
    }

    /**
     * @param currentRating the currentRating to set
     */
    public void setCurrentRating(Rating currentRating) {
        this.currentRating = currentRating;
    }

    /**
     * @return the ratingsMade
     */
    public List<Rating> getRatingsMade() {
        return ratingsMade;
    }

    /**
     * @param ratingsMade the ratingsMade to set
     */
    public void setRatingsMade(List<Rating> ratingsMade) {
        this.ratingsMade = ratingsMade;
    }

    /**
     * @return the ratingsReceived
     */
    public List<Rating> getRatingsReceived() {
        return ratingsReceived;
    }

    /**
     * @param ratingsReceived the ratingsReceived to set
     */
    public void setRatingsReceived(List<Rating> ratingsReceived) {
        this.ratingsReceived = ratingsReceived;
    }

    /**
     * @return the friends
     */
    public List<Player> getFriends() {
        return friends;
    }

    /**
     * @param friends the friends to set
     */
    public void setFriends(List<Player> friends) {
        this.friends = friends;
    }

    /**
     * @return the games
     */
    public List<Game> getGames() {
        return games;
    }

    /**
     * @param games the games to set
     */
    public void setGames(List<Game> games) {
        this.games = games;
    }

    /**
     * @return the sentFriendRequests
     */
    public List<FriendRequest> getSentFriendRequests() {
        return sentFriendRequests;
    }

    /**
     * @param sentFriendRequests the sentFriendRequests to set
     */
    public void setSentFriendRequests(List<FriendRequest> sentFriendRequests) {
        this.sentFriendRequests = sentFriendRequests;
    }

    /**
     * @return the receivedFriendRequests
     */
    public List<FriendRequest> getReceivedFriendRequests() {
        return receivedFriendRequests;
    }

    /**
     * @param receivedFriendRequests the receivedFriendRequests to set
     */
    public void setReceivedFriendRequests(List<FriendRequest> receivedFriendRequests) {
        this.receivedFriendRequests = receivedFriendRequests;
    }

    /**
     * @return the posts
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * @param posts the posts to set
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    /**
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * @return the passwordResets
     */
    public List<PasswordReset> getPasswordResets() {
        return passwordResets;
    }

    /**
     * @param passwordResets the passwordResets to set
     */
    public void setPasswordResets(List<PasswordReset> passwordResets) {
        this.passwordResets = passwordResets;
    }

    /**
     * @return the hasCompletedQuiz
     */
    public Boolean getHasCompletedQuiz() {
        return hasCompletedQuiz;
    }

    /**
     * @param hasCompletedQuiz the hasCompletedQuiz to set
     */
    public void setHasCompletedQuiz(Boolean hasCompletedQuiz) {
        this.hasCompletedQuiz = hasCompletedQuiz;
    }

    /**
     * @return the battleTag
     */
    public String getBattleTag() {
        return battleTag;
    }

    /**
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @return the originId
     */
    public String getOriginId() {
        return originId;
    }

    /**
     * @return the steamId
     */
    public String getSteamId() {
        return steamId;
    }

    /**
     * @param battleTag the battleTag to set
     */
    public void setBattleTag(String battleTag) {
        this.battleTag = battleTag;
    }

    /**
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @param originId the originId to set
     */
    public void setOriginId(String originId) {
        this.originId = originId;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param steamId the steamId to set
     */
    public void setSteamId(String steamId) {
        this.steamId = steamId;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
