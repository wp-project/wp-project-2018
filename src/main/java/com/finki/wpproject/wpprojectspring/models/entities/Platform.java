package com.finki.wpproject.wpprojectspring.models.entities;

/**
 * Platform enumeration.
 */
public enum Platform {
    Steam,
    Blizzard,
    Origin
}
