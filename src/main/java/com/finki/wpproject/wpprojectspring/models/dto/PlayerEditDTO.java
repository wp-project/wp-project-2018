package com.finki.wpproject.wpprojectspring.models.dto;

public class PlayerEditDTO {

    private String nickname;
    private String country;
    private String steamId;
    private String originId;
    private String battleTag;

    /**
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the steamId
     */
    public String getSteamId() {
        return steamId;
    }

    /**
     * @param steamId the steamId to set
     */
    public void setSteamId(String steamId) {
        this.steamId = steamId;
    }

    /**
     * @return the originId
     */
    public String getOriginId() {
        return originId;
    }

    /**
     * @param originId the originId to set
     */
    public void setOriginId(String originId) {
        this.originId = originId;
    }

    /**
     * @return the battleTag
     */
    public String getBattleTag() {
        return battleTag;
    }

    /**
     * @param battleTag the battleTag to set
     */
    public void setBattleTag(String battleTag) {
        this.battleTag = battleTag;
    }
}