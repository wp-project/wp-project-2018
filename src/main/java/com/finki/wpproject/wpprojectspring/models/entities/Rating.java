package com.finki.wpproject.wpprojectspring.models.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Rating entity.
 */
@Entity
public class Rating {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Min(value = -5)
    @Max(value = 5)
    private Float solo;
    @NotNull
    @Min(value = -5)
    @Max(value = 5)
    private Float skill;
    @NotNull
    @Min(value = -5)
    @Max(value = 5)
    private Float speech;
    @NotNull
    @Min(value = -5)
    @Max(value = 5)
    private Float spirit;
    @NotNull
    @Min(value = -5)
    @Max(value = 5)
    private Float sophistication;

    @ManyToOne
    private Player madeBy;
    @ManyToOne
    private Player relatedTo;

    private LocalDateTime madeOn;

    public Rating() {
        solo = 0f;
        speech = 0f;
        spirit = 0f;
        sophistication = 0f;
        skill = 0f;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the madeBy
     */
    public Player getMadeBy() {
        return madeBy;
    }

    /**
     * @param madeBy the madeBy to set
     */
    public void setMadeBy(Player madeBy) {
        this.madeBy = madeBy;
    }

    /**
     * @return the madeOn
     */
    public LocalDateTime getMadeOn() {
        return madeOn;
    }

    /**
     * @param madeOn the madeOn to set
     */
    public void setMadeOn(LocalDateTime madeOn) {
        this.madeOn = madeOn;
    }

    /**
     * @return the relatedTo
     */
    public Player getRelatedTo() {
        return relatedTo;
    }

    /**
     * @param relatedTo the relatedTo to set
     */
    public void setRelatedTo(Player relatedTo) {
        this.relatedTo = relatedTo;
    }

    /**
     * @return the skill
     */
    public Float getSkill() {
        return skill;
    }

    /**
     * @param skill the skill to set
     */
    public void setSkill(Float skill) {
        this.skill = skill;
    }

    /**
     * @return the solo
     */
    public Float getSolo() {
        return solo;
    }

    /**
     * @param solo the solo to set
     */
    public void setSolo(Float solo) {
        this.solo = solo;
    }

    /**
     * @return the sophistication
     */
    public Float getSophistication() {
        return sophistication;
    }

    /**
     * @param sophistication the sophistication to set
     */
    public void setSophistication(Float sophistication) {
        this.sophistication = sophistication;
    }

    /**
     * @return the speech
     */
    public Float getSpeech() {
        return speech;
    }

    /**
     * @param speech the speech to set
     */
    public void setSpeech(Float speech) {
        this.speech = speech;
    }

    /**
     * @return the spirit
     */
    public Float getSpirit() {
        return spirit;
    }

    /**
     * @param spirit the spirit to set
     */
    public void setSpirit(Float spirit) {
        this.spirit = spirit;
    }
}
