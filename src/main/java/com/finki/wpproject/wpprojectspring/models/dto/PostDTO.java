package com.finki.wpproject.wpprojectspring.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PostDTO {

    @NotNull
    @Size(min = 2, max = 160)
    private String text;
    private String link;
    private String username;
    private String postedOn;

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the postedOn
     */
    public String getPostedOn() {
        return postedOn;
    }

    /**
     * @param postedOn the postedOn to set
     */
    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }
}