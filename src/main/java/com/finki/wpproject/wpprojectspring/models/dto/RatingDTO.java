package com.finki.wpproject.wpprojectspring.models.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * DTO class which represents a rating.
 */
public class RatingDTO {
    
    @Max(5)
    @Min(-5)
    private Float solo;

    @Max(5)
    @Min(-5)
    private Float speech;

    @Max(5)
    @Min(-5)
    private Float spirit;

    @Max(5)
    @Min(-5)
    private Float skill;
    
    @Max(5)
    @Min(-5)
    private Float sophistication;

    /**
     * @return the solo
     */
    public Float getSolo() {
        return solo;
    }

    /**
     * @param solo the solo to set
     */
    public void setSolo(Float solo) {
        this.solo = solo;
    }

    /**
     * @return the speech
     */
    public Float getSpeech() {
        return speech;
    }

    /**
     * @param speech the speech to set
     */
    public void setSpeech(Float speech) {
        this.speech = speech;
    }

    /**
     * @return the spirit
     */
    public Float getSpirit() {
        return spirit;
    }

    /**
     * @param spirit the spirit to set
     */
    public void setSpirit(Float spirit) {
        this.spirit = spirit;
    }

    /**
     * @return the skill
     */
    public Float getSkill() {
        return skill;
    }

    /**
     * @param skill the skill to set
     */
    public void setSkill(Float skill) {
        this.skill = skill;
    }

    /**
     * @return the sophistication
     */
    public Float getSophistication() {
        return sophistication;
    }

    /**
     * @param sophistication the sophistication to set
     */
    public void setSophistication(Float sophistication) {
        this.sophistication = sophistication;
    }
}
