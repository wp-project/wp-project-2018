package com.finki.wpproject.wpprojectspring.models.dto;

import java.util.List;

/**
 * DTO class for quiz answers.
 */
public class QuizAnswersDTO {
    
    private List<Float> soloAnswers;
    private List<Float> speechAnswers;
    private List<Float> spiritAnswers;
    private List<Float> sophAnswers;

    /**
     * @return the soloAnswers
     */
    public List<Float> getSoloAnswers() {
        return soloAnswers;
    }

    /**
     * @param soloAnswers the soloAnswers to set
     */
    public void setSoloAnswers(List<Float> soloAnswers) {
        this.soloAnswers = soloAnswers;
    }

    /**
     * @return the speechAnswers
     */
    public List<Float> getSpeechAnswers() {
        return speechAnswers;
    }

    /**
     * @param speechAnswers the speechAnswers to set
     */
    public void setSpeechAnswers(List<Float> speechAnswers) {
        this.speechAnswers = speechAnswers;
    }

    /**
     * @return the spiritAnswers
     */
    public List<Float> getSpiritAnswers() {
        return spiritAnswers;
    }

    /**
     * @param spiritAnswers the spiritAnswers to set
     */
    public void setSpiritAnswers(List<Float> spiritAnswers) {
        this.spiritAnswers = spiritAnswers;
    }

    /**
     * @return the sophAnswers
     */
    public List<Float> getSophAnswers() {
        return sophAnswers;
    }

    /**
     * @param sophAnswers the sophAnswers to set
     */
    public void setSophAnswers(List<Float> sophAnswers) {
        this.sophAnswers = sophAnswers;
    }
}
