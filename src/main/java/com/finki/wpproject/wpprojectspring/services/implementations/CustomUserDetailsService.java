package com.finki.wpproject.wpprojectspring.services.implementations;

import java.util.Optional;

import com.finki.wpproject.wpprojectspring.models.entities.Player;
import com.finki.wpproject.wpprojectspring.persistence.PlayerRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Service responsible for loading players by username.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public PlayerRepository playerRepository;

    /**
     * Loads a player with a given username, or throws an exception if no user exists.
     * @param s - The username of the desired player to load.
     * @return - The user details related to the player with the given username.
     * @throws UsernameNotFoundException - When a player with such a username does not exist
     */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        logger.info("Trying to load user by username '{}'.", s);
        Optional<Player> player = playerRepository.findPlayerByUsername(s);
        if (player.isPresent()) {
            logger.info("Loading user '{}' successful.", s);
            return player.get();
        }
        logger.error("Username '{}' does not exist.", s);
        throw new UsernameNotFoundException(s);
    }
}
