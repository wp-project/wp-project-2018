package com.finki.wpproject.wpprojectspring.services.implementations;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.finki.wpproject.wpprojectspring.exceptions.common.BadRequestException;
import com.finki.wpproject.wpprojectspring.exceptions.post.NoPostsException;
import com.finki.wpproject.wpprojectspring.models.dto.PostDTO;
import com.finki.wpproject.wpprojectspring.models.entities.Player;
import com.finki.wpproject.wpprojectspring.models.entities.Post;
import com.finki.wpproject.wpprojectspring.persistence.PlayerRepository;
import com.finki.wpproject.wpprojectspring.persistence.PostRepository;
import com.finki.wpproject.wpprojectspring.services.IPostService;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service
public class PostService implements IPostService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private PlayerRepository playerRepository;
    private PostRepository postRepository;
    private ModelMapper modelMapper;

    public PostService(PlayerRepository playerRepository, PostRepository postRepository, ModelMapper modelMapper) {
        this.playerRepository = playerRepository;
        this.postRepository = postRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public PostDTO newPost(PostDTO newPost, BindingResult bindingResult) {
        
        logger.info("New post request.");

        if (bindingResult.hasErrors() || (newPost.getLink() != null && !newPost.getLink().isEmpty()
                && !newPost.getLink().matches("^http(.*.(jpg|jpeg|png|gif).*)"))) {
            logger.error("Invalid post request.");
            throw new BadRequestException();
        }

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!username.equals(newPost.getUsername())) {
            logger.error("Post request is not from logged in user.");
            throw new BadRequestException();
        }

        Player player = playerRepository.findPlayerByUsername(username).get();
        Post post = modelMapper.map(newPost, Post.class);
        post.setOp(player);
        post.setPostedOn(LocalDateTime.now());

        postRepository.save(post);

        newPost.setPostedOn(post.getPostedOn().format(DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm")));

        return newPost;
    }

    @Override
    public List<PostDTO> getPostsForUser(String username) {

        if (username == null) {
            throw new BadRequestException("Parameter username is missing.");
        }

        logger.info("Posts requested for player '{}'", username);
        Optional<Player> playerOptional = playerRepository.findPlayerByUsername(username);
        if (!playerOptional.isPresent()) {
            logger.error("Username '{}' does not exist, cannot retrieve ratings.", username);
            throw new UsernameNotFoundException(username);
        }

        Player player = playerOptional.get();
        if (player.getPosts() == null || player.getPosts().size() == 0) {
            logger.error("User with username '{}' has no posts.", username);
            throw new NoPostsException();
        }

        logger.info("Converting posts to PostDTO objects.");
        List<PostDTO> resultList = new ArrayList<>();
        for (Post post : player.getPosts()) {
            PostDTO postDTO = modelMapper.map(post, PostDTO.class);
            postDTO.setUsername(username);
            resultList.add(postDTO);
        }
        logger.info("Reversing result list.");
        Collections.reverse(resultList);

        return resultList;
    }

    @Override
    public List<PostDTO> getPostsOfFriends() {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Fetching most recent posts of friends of user {}", username);

        Player loggedInPlayer = this.playerRepository.findPlayerByUsername(username).get();

        List<PostDTO> postsOfFriends = loggedInPlayer.getFriends().stream()
                .map(friend -> friend.getPosts().stream()
                        .filter(post -> post.getPostedOn().isAfter(LocalDateTime.now().minusDays(1))))
                .flatMap(posts -> posts.map(post -> modelMapper.map(post, PostDTO.class))).collect(Collectors.toList());
        logger.info("Found {} new posts from friends of user {}", postsOfFriends.size(), username);

        return postsOfFriends;
    }
}