package com.finki.wpproject.wpprojectspring.services;

import com.finki.wpproject.wpprojectspring.models.dto.QuizAnswersDTO;
import com.finki.wpproject.wpprojectspring.models.dto.RatingDTO;

import org.springframework.validation.BindingResult;

public interface IRatingService {

    /**
     * Method for handling quiz answers data posted by the frontend.
     *
     * @param quizAnswersDTO - The quiz answers data.
     * @param bindingResult  - Object containing the validity of the quiz answers data.
     * @return - The rating calculated from the quiz answers data.
     */
    public RatingDTO submitQuizAnswers(QuizAnswersDTO quizAnswersDTO, BindingResult bindingResult);

    /**
     * Method that fetches and returns the current rating of a given user.
     * @param username - The query parameter name for the username whose ratings we want to get
     * @return - The current rating of the user with the given username
     */
    public RatingDTO getRatingForUser(String username);

    /**
     * Method that checks whether a user has been rated by the logged-in user.
     * @param username - The username of the user to be checked.
     * @return - True if rated, false if not.
     */
    public boolean isRated(String username);

    /**
     * Method that adds a new rating from the logged in user for another player.
     * @param username - The username of the other player.
     * @param rating - The rating to be added.
     * @param bindingResult - Result of parameter binding.
     */
    public void ratePlayer(String username, RatingDTO rating, BindingResult bindingResult);
}