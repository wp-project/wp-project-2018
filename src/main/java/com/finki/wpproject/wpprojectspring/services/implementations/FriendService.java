package com.finki.wpproject.wpprojectspring.services.implementations;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.finki.wpproject.wpprojectspring.exceptions.common.BadRequestException;
import com.finki.wpproject.wpprojectspring.exceptions.friendrequest.AlreadyFriendsException;
import com.finki.wpproject.wpprojectspring.exceptions.friendrequest.FriendRequestAlreadyExistsException;
import com.finki.wpproject.wpprojectspring.exceptions.friendrequest.FriendRequestNotFoundException;
import com.finki.wpproject.wpprojectspring.models.dto.FriendRequestDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerDTO;
import com.finki.wpproject.wpprojectspring.models.entities.FriendRequest;
import com.finki.wpproject.wpprojectspring.models.entities.Player;
import com.finki.wpproject.wpprojectspring.persistence.FriendRequestRepository;
import com.finki.wpproject.wpprojectspring.persistence.PlayerRepository;
import com.finki.wpproject.wpprojectspring.services.IFriendService;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class FriendService implements IFriendService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private PlayerRepository playerRepository;
    private FriendRequestRepository friendRequestRepository;
    private ModelMapper modelMapper;

    public FriendService(PlayerRepository playerRepository, FriendRequestRepository friendRequestRepository,
            ModelMapper modelMapper) {
        this.playerRepository = playerRepository;
        this.friendRequestRepository = friendRequestRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public boolean isFriend(String username) {
        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Checking if user with username '{}' is friends with logged in user '{}'.", username, loggedInUser);

        Player player = playerRepository.findPlayerByUsername(loggedInUser).get();

        return player.getFriends().stream().anyMatch(p -> p.getUsername().equals(username));
    }

    @Override
    public boolean hasFriendRequest(String username) {
        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Checking if user with username '{}' has a friend request from user '{}'.", username, loggedInUser);

        Player player = playerRepository.findPlayerByUsername(loggedInUser).get();

        return player.getSentFriendRequests().stream()
                .anyMatch(fr -> fr.getSentTo().getUsername().equals(username) && fr.getPending() == true)
                || player.getReceivedFriendRequests().stream()
                        .anyMatch(fr -> fr.getSentFrom().getUsername().equals(username) && fr.getPending() == true);
    }

    @Override
    public boolean addFriendRequest(String username) {
        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Creating a friend request between users with username '{}' and '{}'.", username, loggedInUser);

        Optional<Player> otherPlayerOptional = playerRepository.findPlayerByUsername(username);
        if (!otherPlayerOptional.isPresent()) {
            throw new UsernameNotFoundException(username);
        }

        if (hasFriendRequest(username)) {
            throw new FriendRequestAlreadyExistsException(loggedInUser, username);
        }

        if (isFriend(username)) {
            throw new AlreadyFriendsException(loggedInUser, username);
        }

        Player otherPlayer = otherPlayerOptional.get();
        Player loggedInPlayer = playerRepository.findPlayerByUsername(loggedInUser).get();

        FriendRequest friendRequest = new FriendRequest(loggedInPlayer, otherPlayer);

        friendRequestRepository.save(friendRequest);
        logger.info("Friend request created successfully.");

        return true;
    }

    @Override
    public int getUnconfirmedFriendRequestCount() {
        Player loggedInPlayer = playerRepository
                .findPlayerByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        logger.info("Counting unconfirmed friend requests for user '{}'.", loggedInPlayer.getUsername());

        int numberOfFRs = loggedInPlayer.getReceivedFriendRequests().size() - loggedInPlayer.getFriends().size();
        logger.info("Successfully retrieved {} unconfirmed friend requests for user '{}'.", numberOfFRs,
                loggedInPlayer.getUsername());
        return numberOfFRs;
    }

    @Override
    public List<FriendRequestDTO> getPendingFriendRequests() {
        Player loggedInPlayer = playerRepository
                .findPlayerByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        logger.info("Acquiring all pending friend requests from user '{}'.", loggedInPlayer.getUsername());

        List<FriendRequestDTO> frs = loggedInPlayer.getReceivedFriendRequests().stream().filter(fr -> fr.getPending())
                .map(fr -> modelMapper.map(fr, FriendRequestDTO.class)).collect(Collectors.toList());

        logger.info("Acquired {} pending friend requests for user '{}'.", frs.size(), loggedInPlayer.getUsername());

        return frs;
    }

    @Override
    public void resolveFriendRequest(Long id, Boolean resolution) {
        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Resolving friend request with id '{}' as '{}' for user '{}'.", id, resolution, loggedInUser);

        Optional<FriendRequest> friendRequestOptional = friendRequestRepository.findFriendRequestById(id);
        if (!friendRequestOptional.isPresent()) {
            logger.error("Friend request with id '{}' does not exist.", id);
            throw new FriendRequestNotFoundException(id);
        }

        FriendRequest friendRequest = friendRequestOptional.get();
        if (!friendRequest.getSentTo().getUsername().equals(loggedInUser)) {
            logger.error("Friend request is not meant for the user trying to resolve it.");
            throw new BadRequestException();
        }

        friendRequest.resolve(resolution);
        logger.info("Friend request resolved.");

        friendRequestRepository.save(friendRequest);

        if (resolution == true) {
            friendRequest.getSentFrom().getFriends().add(friendRequest.getSentTo());
            playerRepository.save(friendRequest.getSentFrom());
            friendRequest.getSentTo().getFriends().add(friendRequest.getSentFrom());
            playerRepository.save(friendRequest.getSentTo());
            logger.info("User '{}' added as friend of user '{}'.", friendRequest.getSentTo().getUsername(),
                    friendRequest.getSentFrom().getUsername());
        }
    }

    @Override
    public List<PlayerDTO> getFriends() {

        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Getting all friends of player '{}'.", loggedInUser);

        Player loggedInPlayer = playerRepository.findPlayerByUsername(loggedInUser).get();

        return loggedInPlayer.getFriends().stream().map(friend -> modelMapper.map(friend, PlayerDTO.class))
                .collect(Collectors.toList());
    }
}