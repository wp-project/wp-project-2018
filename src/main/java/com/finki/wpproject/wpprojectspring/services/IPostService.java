package com.finki.wpproject.wpprojectspring.services;

import java.util.List;

import com.finki.wpproject.wpprojectspring.models.dto.PostDTO;

import org.springframework.validation.BindingResult;

public interface IPostService {

    /**
     * Method for creating a new post.
     */
    public PostDTO newPost(PostDTO newPost, BindingResult bindingResult);

    /**
     * Method for getting all posts from a user.
     */
    public List<PostDTO> getPostsForUser(String username);

    /**
     * Method that finds and returns most recent posts from the logged in users friend list.
     */
    public List<PostDTO> getPostsOfFriends();

}