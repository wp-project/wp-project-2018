package com.finki.wpproject.wpprojectspring.services.implementations;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.finki.wpproject.wpprojectspring.exceptions.common.BadRequestException;
import com.finki.wpproject.wpprojectspring.exceptions.common.UnauthorizedException;
import com.finki.wpproject.wpprojectspring.exceptions.player.UnableToUpdatePlayerException;
import com.finki.wpproject.wpprojectspring.exceptions.rating.InvalidQuizAnswers;
import com.finki.wpproject.wpprojectspring.exceptions.rating.NoRatingException;
import com.finki.wpproject.wpprojectspring.exceptions.rating.SecondQuizAttemptException;
import com.finki.wpproject.wpprojectspring.exceptions.rating.UnableToSaveRatingException;
import com.finki.wpproject.wpprojectspring.models.dto.QuizAnswersDTO;
import com.finki.wpproject.wpprojectspring.models.dto.RatingDTO;
import com.finki.wpproject.wpprojectspring.models.entities.Player;
import com.finki.wpproject.wpprojectspring.models.entities.Rating;
import com.finki.wpproject.wpprojectspring.persistence.PlayerRepository;
import com.finki.wpproject.wpprojectspring.persistence.RatingRepository;
import com.finki.wpproject.wpprojectspring.services.IFriendService;
import com.finki.wpproject.wpprojectspring.services.IRatingService;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service
public class RatingService implements IRatingService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private RatingRepository ratingRepository;
    private PlayerRepository playerRepository;
    private ModelMapper modelMapper;
    private IFriendService friendService;

    public RatingService(RatingRepository ratingRepository, PlayerRepository playerRepository,
            ModelMapper modelMapper, IFriendService friendService) {
        this.ratingRepository = ratingRepository;
        this.playerRepository = playerRepository;
        this.modelMapper = modelMapper;
        this.friendService = friendService;
    }

    @Override
    public RatingDTO submitQuizAnswers(QuizAnswersDTO quizAnswersDTO, BindingResult bindingResult) {

        logger.info("Quiz answers submitted.");

        SecurityContext context = SecurityContextHolder.getContext();
        if (!context.getAuthentication().isAuthenticated()) {
            logger.error("User is not authenticated.");
            throw new UnauthorizedException();
        }

        Player player = playerRepository.findPlayerByUsername(context.getAuthentication().getName()).get();

        logger.info("Checking if user '{}' has already submitted quiz answers.", player.getUsername());
        if (player.getHasCompletedQuiz()) {
            logger.error("User '{}', has already submitted quiz answers, aborting.", player.getUsername());
            throw new SecondQuizAttemptException();
        }

        if (bindingResult.hasErrors()) {
            logger.error("Quiz answers have binding errors.");
            throw new InvalidQuizAnswers();
        }

        logger.info("Calculating rating according to quiz answers.");
        Rating selfRating = calculateRating(quizAnswersDTO);
        selfRating.setRelatedTo(player);
        selfRating.setMadeBy(player);

        logger.info("Trying to save new self rating for user '{}'.", player.getUsername());
        Optional<Rating> ratingOptional = ratingRepository.save(selfRating);
        if (!ratingOptional.isPresent()) {
            logger.error("Unable to save rating.");
            throw new UnableToSaveRatingException();
        }

        logger.info("Updating that the player '{}' has completed the quiz.", player.getUsername());
        player.setHasCompletedQuiz(true);
        Optional<Player> playerOptional = playerRepository.save(player);
        if (!playerOptional.isPresent()) {
            throw new UnableToUpdatePlayerException();
        }
        player = playerOptional.get();

        logger.info("Updating player '{}' current rating.", player.getUsername());
        updateRating(player);

        logger.info("Returning the newly added rating.");
        return modelMapper.map(ratingOptional.get(), RatingDTO.class);
    }

    @Override
    public RatingDTO getRatingForUser(String username) {

        if (username == null) {
            throw new BadRequestException("Parameter username is missing.");
        }

        logger.info("Ratings requested for player '{}'", username);
        Optional<Player> playerOptional = playerRepository.findPlayerByUsername(username);
        if (!playerOptional.isPresent()) {
            logger.error("Username '{}' does not exist, cannot retrieve ratings.", username);
            throw new UsernameNotFoundException(username);
        }

        Player player = playerOptional.get();
        Rating currentRating = player.getCurrentRating();
        if (currentRating == null) {
            logger.error("User with username '{}' does not have a rating yet.", username);
            throw new NoRatingException();
        }
        logger.info("Fetching and mapping ratings to ratings dto.");
        RatingDTO ratingDTO = modelMapper.map(currentRating, RatingDTO.class);
        return ratingDTO;
    }

    @Override
    public boolean isRated(String username) {

        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        logger.info("Checking if user with username '{}' is rated by logged in user '{}'.", username, loggedInUser);

        Player player = playerRepository.findPlayerByUsername(loggedInUser).get();

        return hasRating(player, username);
    }

    @Override
    public void ratePlayer(String username, RatingDTO rating, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            logger.error("Error when binding rating in ratePlayer.");
            throw new BadRequestException("Binding errors.");
        }

        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();
        if (loggedInUser.equals(username)) {
            logger.error("User '{}' trying to rate him/herself.", username);
            throw new BadRequestException("Unable to rate yourself.");
        }
        
        logger.info("Adding new rating for user '{}' from user '{}'.", username, loggedInUser);

        Player loggedInPlayer = this.playerRepository.findPlayerByUsername(loggedInUser).get();
        Optional<Player> toBeRatedOptional = this.playerRepository.findPlayerByUsername(username);
        if (!toBeRatedOptional.isPresent()) {
            logger.error("Trying to rate player that doesn't exist.");
            throw new UsernameNotFoundException(username);
        }
        Player toBeRated = toBeRatedOptional.get();

        if (loggedInPlayer.getFriends().stream().noneMatch(friend -> friend.getUsername().equals(username))) {
            logger.error("Trying to rate player that isn't a friend.");
            throw new BadRequestException("Unable to rate player which isn't a friend.");
        }

        if (hasRating(loggedInPlayer, username)) {
            logger.error("Trying to rate a player which was already rated.");
            throw new BadRequestException("Unable to rate a player twice.");
        }

        Rating toBeSaved = modelMapper.map(rating, Rating.class);
        toBeSaved.setMadeBy(loggedInPlayer);
        toBeSaved.setRelatedTo(toBeRated);
        toBeSaved.setMadeOn(LocalDateTime.now());

        this.ratingRepository.save(toBeSaved);

        updateRating(toBeRated);
    }

    /**
     * Given quiz answers, calculates and returns the corresponding rating.
     * @param quizAnswersDTO - Submitted quiz answers.
     * @return - The calculated rating.
     */
    private Rating calculateRating(QuizAnswersDTO quizAnswersDTO) {

        Rating newRating = new Rating();

        newRating.setSolo(average(quizAnswersDTO.getSoloAnswers()));
        newRating.setSpeech(average(quizAnswersDTO.getSpeechAnswers()));
        newRating.setSpirit(average(quizAnswersDTO.getSpiritAnswers()));
        newRating.setSophistication(average(quizAnswersDTO.getSophAnswers()));
        newRating.setSkill(0f);

        newRating.setMadeOn(LocalDateTime.now());

        return newRating;
    }

    /**
     * Method that renews the players current rating. Should be called after each new rating entry.
     *
     * @param player - The player which rating should be updated.
     * @return - The updated player.
     */
    private Player updateRating(Player player) {

        Rating newRating;
        if (player.getCurrentRating() == null) {
            newRating = new Rating();
        } else {
            newRating = player.getCurrentRating();
        }

        logger.info("Fething all ratings the player '{}' has received.", player.getUsername());
        List<Rating> allRatings = player.getRatingsReceived();

        logger.info("Averaging all ratings for player '{}'.", player.getUsername());
        ratingAverage(allRatings, newRating);

        logger.info("Trying to save/update the newly calculated rating.");
        Optional<Rating> ratingOptional = ratingRepository.save(newRating);
        if (!ratingOptional.isPresent()) {
            logger.error("Unable to save/update the newly calculated rating.");
            throw new UnableToSaveRatingException();
        }

        player.setCurrentRating(ratingOptional.get());
        logger.info("Updating player '{}' with the new current rating.", player.getUsername());
        Optional<Player> playerOptional = playerRepository.save(player);
        if (!playerOptional.isPresent()) {
            logger.error("Failed to update player with the new rating average.");
            throw new UnableToUpdatePlayerException();
        }

        return player;
    }

    /**
     * Helper function which calculates the average of a list of floats.
     *
     * @param list - The list of float values.
     * @return = The average of the list given as an argument.
     */
    private float average(List<Float> list) {

        float average = 0;
        for (Float element : list) {
            average += element;
        }

        return average / list.size();
    }

    /**
     * Helper function for calculating an average of all stats from a list of ratings.
     *
     * @param list      - The list of ratings which should be averaged.
     * @param oldRating - An object which will be filled with the averages. E.g. transient database object.
     * @return - The updated old rating.
     */
    private Rating ratingAverage(List<Rating> list, Rating oldRating) {

        float solo = 0, speech = 0, skill = 0, spirit = 0, sophistication = 0;

        logger.info("Summing all rating stats from all ratings.");
        for (Rating rating : list) {
            solo += rating.getSolo();
            speech += rating.getSpeech();
            skill += rating.getSkill();
            spirit += rating.getSpirit();
            sophistication += rating.getSophistication();
        }

        logger.info("Averaging all rating stats.");
        int totalRatings = list.size();
        oldRating.setSolo(solo / totalRatings);
        oldRating.setSpeech(speech / totalRatings);
        oldRating.setSophistication(sophistication / totalRatings);
        oldRating.setSkill(skill / totalRatings);
        oldRating.setSpirit(spirit / totalRatings);

        return oldRating;
    }

    /**
     * Method that checks whether a player has a rating for user with given username.
     * @param player - The player to be checked.
     * @param username - The username of the other player.
     * @return - True if has, false if hasn't.
     */
    private boolean hasRating(Player player, String username) {
        return player.getRatingsMade().stream().anyMatch(r -> r.getRelatedTo().getUsername().equals(username));
    }
}