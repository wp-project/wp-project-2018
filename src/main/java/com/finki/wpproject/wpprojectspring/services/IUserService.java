package com.finki.wpproject.wpprojectspring.services;

import java.util.List;

import javax.validation.Valid;

import com.finki.wpproject.wpprojectspring.models.dto.LoggedInUserDTO;
import com.finki.wpproject.wpprojectspring.models.dto.MatchedPlayer;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerEditDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerQuery;

import org.springframework.validation.BindingResult;

public interface IUserService {

    /**
     * Method for registering a new player.
     * @param player - The player info DTO.
     * @param bindingResult - Validity of the player DTO.
     * @return - The registered player DTO equivalent.
     */
    public PlayerDTO registerPlayer(PlayerDTO player, BindingResult bindingResult);

    /**
     * Method which returns info about the logged in user.
     * @return - A LoggedInUserDto object which contains the username and a flag of whether the user has completed the quiz or not.
     */
    public LoggedInUserDTO getLoggedInUser();

    /**
     * Method which finds users that match the search term.
     * @return - A list containing the usernames of all the users which fulfill the condition.
     */
    public List<PlayerDTO> findUsers(String term);

    /**
     * Method which finds a player by username.
     * @param username - The username to search with.
     * @return - The player dto for the found user.
     */
    public PlayerDTO findUserByUsername(String username);

    /**
     * Method that matches players by a given query (stats and country).
     * @param playerQuery - The query which needs to be matched.
     * @return - A list of 5 players which best match those criteria.
     */
    public List<MatchedPlayer> matchPlayers(@Valid PlayerQuery playerQuery, BindingResult bindingResult);

    /**
     * Method that edits an existing player.
     * @param player - The player to be edited.
     */
	public void editPlayer(PlayerEditDTO player, BindingResult bindingResult);

}