package com.finki.wpproject.wpprojectspring.services.implementations;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.finki.wpproject.wpprojectspring.exceptions.common.BadRequestException;
import com.finki.wpproject.wpprojectspring.exceptions.common.UnauthorizedException;
import com.finki.wpproject.wpprojectspring.exceptions.player.PlayerExistsException;
import com.finki.wpproject.wpprojectspring.exceptions.player.UnableToRegisterPlayerException;
import com.finki.wpproject.wpprojectspring.models.dto.LoggedInUserDTO;
import com.finki.wpproject.wpprojectspring.models.dto.MatchedPlayer;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerEditDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerQuery;
import com.finki.wpproject.wpprojectspring.models.entities.Player;
import com.finki.wpproject.wpprojectspring.models.entities.Rating;
import com.finki.wpproject.wpprojectspring.persistence.PlayerRepository;
import com.finki.wpproject.wpprojectspring.services.IUserService;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service
public class UserService implements IUserService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private PlayerRepository playerRepository;
    private PasswordEncoder passwordEncoder;
    private ModelMapper modelMapper;

    public UserService(PlayerRepository playerRepository, PasswordEncoder passwordEncoder, ModelMapper modelMapper) {
        this.playerRepository = playerRepository;
        this.passwordEncoder = passwordEncoder;
        this.modelMapper = modelMapper;
    }

    @Override
    public PlayerDTO registerPlayer(PlayerDTO player, BindingResult bindingResult) {

        logger.info("Start register new player.");
        if (bindingResult.hasErrors() || !player.getPassword()
                .matches("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[.,$!@#])[a-zA-Z0-9.,$!@#]{8,20}")) {
            logger.error("Invalid fields in register form.");
            throw new UnableToRegisterPlayerException();
        }

        logger.info("Checking if player '{}' already exists.", player.getUsername());
        Optional<Player> existingPlayer = playerRepository.findPlayerByUsername(player.getUsername());
        if (existingPlayer.isPresent()) {
            logger.error("Player '{}' already exists, unable to register new player.", player.getUsername());
            throw new PlayerExistsException(player.getUsername());
        }

        logger.info("Encrypting password.");
        player.setPassword(passwordEncoder.encode(player.getUsername()));
        logger.info("Mapping player dto to player object.");
        Player toSave = modelMapper.map(player, Player.class);
        logger.info("Saving player '{}' in database.", player.getUsername());
        Optional<Player> savedPlayer = playerRepository.save(toSave);
        if (!savedPlayer.isPresent()) {
            logger.error("Unable to save player '{}'.", player.getUsername());
            throw new UnableToRegisterPlayerException(player.getUsername());
        }

        logger.info("Mapping player to player dto object and returning.");
        PlayerDTO registeredPlayer = modelMapper.map(savedPlayer.get(), PlayerDTO.class);

        return registeredPlayer;
    }

    @Override
    public LoggedInUserDTO getLoggedInUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        logger.info("Checking if player is logged in.");
        if (authentication == null || !authentication.isAuthenticated()) {
            throw new UnauthorizedException();
        }

        Player loggedInPlayer = playerRepository.findPlayerByUsername(authentication.getName()).get();
        LoggedInUserDTO loggedInUserDTO = modelMapper.map(loggedInPlayer, LoggedInUserDTO.class);

        return loggedInUserDTO;
    }

    @Override
    public List<PlayerDTO> findUsers(String term) {

        logger.info("Searching for players by term: '{}'", term);

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<PlayerDTO> result = playerRepository.findPlayersByTerm(term).stream()
                .map(player -> modelMapper.map(player, PlayerDTO.class))
                .filter(player -> !player.getUsername().equals(username)).collect(Collectors.toList());

        return result;
    }

    @Override
    public PlayerDTO findUserByUsername(String username) {

        Optional<Player> player = playerRepository.findPlayerByUsername(username);

        if (!player.isPresent()) {
            throw new UsernameNotFoundException(username);
        }

        return modelMapper.map(player.get(), PlayerDTO.class);
    }

    @Override
    public List<MatchedPlayer> matchPlayers(@Valid PlayerQuery playerQuery, BindingResult bindingResult) {

        logger.info("Matching players by query {}.", playerQuery);

        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        if (bindingResult.hasErrors()) {
            logger.error("Binding errors in player query.");
            throw new BadRequestException();
        }

        List<MatchedPlayer> matchedPlayers = this.playerRepository.findAll().stream()
                .filter(p -> !p.getUsername().equals(username)).map(p -> getMatchedPlayer(p, playerQuery))
                .sorted((p1, p2) -> Float.compare(p1.getMatching(), p2.getMatching())).limit(5)
                .collect(Collectors.toList());

        logger.info("Found {} players matching the given criteria.", matchedPlayers.size());

        return matchedPlayers;
    }

    @Override
    public void editPlayer(PlayerEditDTO player, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            logger.error("Error when binding player dto in editPlayer.");
            throw new BadRequestException("Binding error.");
        }

        String loggedInUser = SecurityContextHolder.getContext().getAuthentication().getName();

        logger.info("Editing player with username '{}'.", loggedInUser);
        Player loggedInPlayer = this.playerRepository.findPlayerByUsername(loggedInUser).get();
        loggedInPlayer.setNickname(player.getNickname());
        loggedInPlayer.setCountry(player.getCountry());
        loggedInPlayer.setBattleTag(player.getBattleTag());
        loggedInPlayer.setSteamId(player.getSteamId());
        loggedInPlayer.setOriginId(player.getOriginId());

        this.playerRepository.save(loggedInPlayer);
    }

    private MatchedPlayer getMatchedPlayer(Player player, PlayerQuery playerQuery) {

        MatchedPlayer matchedPlayer = modelMapper.map(player, MatchedPlayer.class);
        Rating currentRating = player.getCurrentRating();

        float matching = 0f;
        float matchingPerCountry = 10;
        float matchingPerCriteria = (100 - matchingPerCountry) / 5;

        if (currentRating != null) {
            matching += matchingPerCriteria * (10 - Math.abs(currentRating.getSolo() - playerQuery.getSolo())) / 10;
            matching += matchingPerCriteria * (10 - Math.abs(currentRating.getSkill() - playerQuery.getSkill())) / 10;
            matching += matchingPerCriteria * (10 - Math.abs(currentRating.getSpeech() - playerQuery.getSpeech())) / 10;
            matching += matchingPerCriteria * (10 - Math.abs(currentRating.getSpirit() - playerQuery.getSpirit())) / 10;
            matching += matchingPerCriteria
                    * (10 - Math.abs(currentRating.getSophistication() - playerQuery.getSophistication())) / 10;
        }

        matching += matchingPerCountry * (playerQuery.getCountry() == null || playerQuery.getCountry().isEmpty()
                || player.getCountry().equals(playerQuery.getCountry()) ? 1 : 0);

        matchedPlayer.setMatching(matching);

        return matchedPlayer;
    }
}