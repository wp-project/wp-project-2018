package com.finki.wpproject.wpprojectspring.services;

import java.util.List;

import com.finki.wpproject.wpprojectspring.models.dto.FriendRequestDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerDTO;

public interface IFriendService {
    /**
     * Method that checks if the username provided is a friend to the currently logged in user.
     * @param username - The username to be checked.
     * @return - True if friend, false if not.
     */
    public boolean isFriend(String username);

    /**
     * Method that checks whether the user with given username already has a friend request from the logged in user.
     * @param username - The username to be checked.
     * @return - True if already has a friend request, false otherwise
     */
    public boolean hasFriendRequest(String username);

    /**
     * Method which creates and saves a friend request betweeen the user with the given username and the logged-in user.
     * @param username - The username of the other user.
     * @return - True if successfull, false otherwise.
     */
    public boolean addFriendRequest(String username);
    
    /**
     * Method that counts the unconfirmed friend requests that the logged in user has.
     * @return - The number of unconfirmed friend requests.
     */
    public int getUnconfirmedFriendRequestCount();

    /**
     * Method that returns all friend requests received from the logged-in user.
     * @return - A list of all pending friend request.
     */
    public List<FriendRequestDTO> getPendingFriendRequests();

    /**
     * Method that resolves a friend request.
     * @param id - The id of the request.
     * @param resolution - True if accepted, false if declined.
     */
    public void resolveFriendRequest(Long id, Boolean resolution);

    /**
     * Method that returns all friends of the logged-in player.
     */
	public List<PlayerDTO> getFriends();
}