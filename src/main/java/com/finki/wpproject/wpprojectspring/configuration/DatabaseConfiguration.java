package com.finki.wpproject.wpprojectspring.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Class used as central database configuration.
 */
@Configuration
public class DatabaseConfiguration {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Bean
    public DataSource dataSource() {
        logger.info("Configuring data source.");
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .build();
    }
}
