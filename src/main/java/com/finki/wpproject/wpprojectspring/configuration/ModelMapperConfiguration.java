package com.finki.wpproject.wpprojectspring.configuration;

import com.finki.wpproject.wpprojectspring.models.dto.FriendRequestDTO;
import com.finki.wpproject.wpprojectspring.models.dto.LoggedInUserDTO;
import com.finki.wpproject.wpprojectspring.models.dto.MatchedPlayer;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PostDTO;
import com.finki.wpproject.wpprojectspring.models.dto.RatingDTO;
import com.finki.wpproject.wpprojectspring.models.entities.FriendRequest;
import com.finki.wpproject.wpprojectspring.models.entities.Player;
import com.finki.wpproject.wpprojectspring.models.entities.Post;
import com.finki.wpproject.wpprojectspring.models.entities.Rating;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Class used for model mapper configuration.
 */
@Configuration
public class ModelMapperConfiguration {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Bean
    public ModelMapper modelMapper() {
        logger.info("Initializing and configuring the model mapper.");
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setFieldMatchingEnabled(true).setAmbiguityIgnored(true)
                .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PUBLIC)
                .setMatchingStrategy(MatchingStrategies.LOOSE);

        modelMapper.createTypeMap(Player.class, PlayerDTO.class);

        modelMapper.createTypeMap(PlayerDTO.class, Player.class);

        modelMapper.createTypeMap(Player.class, LoggedInUserDTO.class);

        modelMapper.createTypeMap(Rating.class, RatingDTO.class);

        modelMapper.createTypeMap(PostDTO.class, Post.class);

        modelMapper.createTypeMap(Post.class, PostDTO.class);

        modelMapper.createTypeMap(Player.class, MatchedPlayer.class)
                .addMappings(mapper -> mapper.skip(MatchedPlayer::setMatching));

        modelMapper.createTypeMap(FriendRequest.class, FriendRequestDTO.class)
                .addMapping(src -> src.getSentFrom().getUsername(), FriendRequestDTO::setUsername);

        return modelMapper;
    }
}
