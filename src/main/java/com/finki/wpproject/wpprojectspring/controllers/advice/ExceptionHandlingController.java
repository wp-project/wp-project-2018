package com.finki.wpproject.wpprojectspring.controllers.advice;

import com.finki.wpproject.wpprojectspring.exceptions.common.BadRequestException;
import com.finki.wpproject.wpprojectspring.exceptions.common.UnauthorizedException;
import com.finki.wpproject.wpprojectspring.exceptions.friendrequest.AlreadyFriendsException;
import com.finki.wpproject.wpprojectspring.exceptions.friendrequest.FriendRequestAlreadyExistsException;
import com.finki.wpproject.wpprojectspring.exceptions.player.PlayerExistsException;
import com.finki.wpproject.wpprojectspring.exceptions.player.UnableToRegisterPlayerException;
import com.finki.wpproject.wpprojectspring.exceptions.player.UnableToUpdatePlayerException;
import com.finki.wpproject.wpprojectspring.exceptions.rating.InvalidQuizAnswers;
import com.finki.wpproject.wpprojectspring.exceptions.rating.NoRatingException;
import com.finki.wpproject.wpprojectspring.exceptions.rating.SecondQuizAttemptException;
import com.finki.wpproject.wpprojectspring.exceptions.post.NoPostsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Controller advice for handling errors.
 */
@ControllerAdvice
public class ExceptionHandlingController {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Exceptions related to bad requests.
     * @param response - Autowired HttpServletResponse for setting the error code.
     * @param e - Autowired exception that occured, used in logging.
     * @throws IOException
     */
    @ExceptionHandler(value = {
            PlayerExistsException.class,
            UnableToRegisterPlayerException.class,
            InvalidQuizAnswers.class,
            UnableToUpdatePlayerException.class,
            SecondQuizAttemptException.class,
            AlreadyFriendsException.class,
            FriendRequestAlreadyExistsException.class,
            BadRequestException.class})
    public void badRequest(HttpServletResponse response, Exception e) throws IOException {
        logger.error("Bad request.", e);
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }

    /**
     * Exceptions related to unauthorized access.
     * @param response - Autowired HttpServletResponse for setting the error code.
     * @param e - Autowired exception that occured, used in logging.
     * @throws IOException
     */
    @ExceptionHandler(value = {
            UnauthorizedException.class
    })
    public void unauthorized(HttpServletResponse response, Exception e) throws IOException {
        logger.error("Unauthorized access.", e);
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }

    /**
     * Exceptions related to item not found.
     */
    @ExceptionHandler(value = {
        UsernameNotFoundException.class,
        NoRatingException.class,
        NoPostsException.class
    })
    public void notFound(HttpServletResponse response, Exception e) throws IOException {
        logger.error("Not found.", e);
        response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }
}
