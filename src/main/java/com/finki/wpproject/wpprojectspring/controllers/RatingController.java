package com.finki.wpproject.wpprojectspring.controllers;

import javax.validation.Valid;

import com.finki.wpproject.wpprojectspring.models.dto.QuizAnswersDTO;
import com.finki.wpproject.wpprojectspring.models.dto.RatingDTO;
import com.finki.wpproject.wpprojectspring.services.IRatingService;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for handling requests related to ratings.
 */
@RestController
@RequestMapping(value = "api/ratings")
public class RatingController {

    private IRatingService ratingService;

    public RatingController(IRatingService ratingService) {
        this.ratingService = ratingService;
    }

    @PostMapping(value = "quiz", produces = "application/json")
    public RatingDTO submitQuizAnswers(@Valid @RequestBody QuizAnswersDTO quizAnswersDTO, BindingResult bindingResult) {
        return ratingService.submitQuizAnswers(quizAnswersDTO, bindingResult);
    }

    @GetMapping(value = "byUser", produces = "application/json")
    public RatingDTO getRatingForUser(@RequestParam(value = "username", required = true) String username) {
        return ratingService.getRatingForUser(username);
    }

    @GetMapping(value = "isRated")
    public boolean isRated(@RequestParam(required = true, name = "username") String username) {
        return ratingService.isRated(username);
    }

    @PostMapping()
    public void ratePlayer(@RequestParam(required = true, name= "username") String username, @Valid @RequestBody RatingDTO ratingDTO, BindingResult bindingResult) {
        this.ratingService.ratePlayer(username, ratingDTO, bindingResult);
    }
}
