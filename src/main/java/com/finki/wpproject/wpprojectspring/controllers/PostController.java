package com.finki.wpproject.wpprojectspring.controllers;

import java.util.List;

import javax.validation.Valid;

import com.finki.wpproject.wpprojectspring.models.dto.PostDTO;
import com.finki.wpproject.wpprojectspring.services.IPostService;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller responsible for post managing.
 */
@RestController
@RequestMapping(value = "api/posts")
public class PostController {

    private IPostService postService;

    public PostController(IPostService postService) {
        this.postService = postService;
    }

    @PostMapping
    public PostDTO newPost(@Valid @RequestBody PostDTO newPost, BindingResult bindingResult) {
        return postService.newPost(newPost, bindingResult);
    }

    @GetMapping
    public List<PostDTO> getPostsForUser(@RequestParam(value = "username", required = true) String username) {
        return postService.getPostsForUser(username);
    }

    @GetMapping(value = "friends")
    public List<PostDTO> getPostsForFriends() {
        return postService.getPostsOfFriends();
    }
}