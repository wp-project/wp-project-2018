package com.finki.wpproject.wpprojectspring.controllers;

import java.util.List;

import javax.validation.Valid;

import com.finki.wpproject.wpprojectspring.models.dto.LoggedInUserDTO;
import com.finki.wpproject.wpprojectspring.models.dto.MatchedPlayer;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerEditDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerQuery;
import com.finki.wpproject.wpprojectspring.services.IUserService;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller handling requests related to users/players.
 */
@RestController
public class UserController {

    private IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path = "register", method = RequestMethod.POST, produces = "application/json")
    public PlayerDTO registerPlayer(@Valid @RequestBody PlayerDTO player, BindingResult bindingResult) {
        return userService.registerPlayer(player, bindingResult);
    }

    @RequestMapping(path = "loggedInUser", method = RequestMethod.GET, produces = "application/json")
    public LoggedInUserDTO getLoggedInUser() {
        return userService.getLoggedInUser();
    }

    @GetMapping(value = "api/users/findUsers")
    public List<PlayerDTO> findUsers(@RequestParam(required = true) String term) {
        return userService.findUsers(term);
    }

    @GetMapping(value = "api/users")
    public PlayerDTO findUserByUsername(@RequestParam(required = true, name = "username") String username) {
        return userService.findUserByUsername(username);
    }

    @PostMapping(value = "api/users/matchPlayers")
    public List<MatchedPlayer> matchPlayers(@Valid @RequestBody PlayerQuery playerQuery, BindingResult bindingResult) {
        return userService.matchPlayers(playerQuery, bindingResult);
    }

    @PatchMapping(value = "api/users")
    public void editPlayer(@RequestBody PlayerEditDTO player, BindingResult bindingResult) {
        userService.editPlayer(player, bindingResult);
    }
}
