package com.finki.wpproject.wpprojectspring.controllers;

import java.util.List;

import com.finki.wpproject.wpprojectspring.models.dto.FriendRequestDTO;
import com.finki.wpproject.wpprojectspring.models.dto.PlayerDTO;
import com.finki.wpproject.wpprojectspring.services.IFriendService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller handling any requests related to friends.
 */
@RestController
@RequestMapping(value = "api/friends")
public class FriendController {
    private IFriendService friendService;

    public FriendController(IFriendService friendService) {
        this.friendService = friendService;
    }

    @GetMapping(value = "isFriend")
    public boolean isFriend(@RequestParam(required = true, name = "username") String username) {
        return friendService.isFriend(username);
    }

    @GetMapping(value = "hasFriendRequest")
    public boolean hasFriendRequest(@RequestParam(required = true, name = "username") String username) {
        return friendService.hasFriendRequest(username);
    }

    @PostMapping(value = "addFriend")
    public boolean addFriend(@RequestParam(required = true, name = "username") String username) {
        return friendService.addFriendRequest(username);
    }

    @GetMapping(value = "friendRequests")
    public int getUnconfirmedFriendRequestCount() {
        return friendService.getUnconfirmedFriendRequestCount();
    }

    @GetMapping(value = "friendRequestsFull")
    public List<FriendRequestDTO> getPendingFriendRequests() {
        return friendService.getPendingFriendRequests();
    }

    @GetMapping(value = "resolveRequest/{id}")
    public void resolveRequest(@PathVariable Long id,
            @RequestParam(required = true, name = "resolution") Boolean resolution) {
        friendService.resolveFriendRequest(id, resolution);
    }

    @GetMapping
    public List<PlayerDTO> getFriends() {
        return friendService.getFriends();
    }
}