package com.finki.wpproject.wpprojectspring;

import com.finki.wpproject.wpprojectspring.models.entities.Player;
import com.finki.wpproject.wpprojectspring.persistence.InMemoryPlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Main entry point in the spring application.
 */
@EnableWebSecurity
@SpringBootApplication(scanBasePackages = {"com.finki.wpproject"})
public class WpProjectSpringApplication {
    private static final Logger logger = LoggerFactory.getLogger(WpProjectSpringApplication.class);

    /**
     * The main method of the application.
     * @param args - Optional arguments to pass when starting the program.
     */
    public static void main(String[] args) {
        logger.info("Initializing the application.");
        ConfigurableApplicationContext context =
                SpringApplication.run(WpProjectSpringApplication.class, args);

        logger.info("Fetching the player database from the created context.");
        InMemoryPlayerRepository playerRepository = (InMemoryPlayerRepository) context.getBean("playerRepository");

        logger.info("Seeding player data.");
        Player player = new Player();
        player.setUsername("feko");
        player.setPassword(((BCryptPasswordEncoder) context.getBean("passwordEncoder")).encode("Feko123."));
        player.setSteamId(String.format("%d", 76561198036748162L));
        player.setBattleTag("asd#1234");
        player.setOriginId("originId");
        player.setEmail("feko@feko.com");
        player.setNickname("fekos");
        player.setCountry("Macedonia");
        playerRepository.save(player);

        player.setUsername("feko2");
        playerRepository.save(player);
    }
}
